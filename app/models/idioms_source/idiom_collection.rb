# frozen_string_literal: true

module IdiomsSource
  class IdiomCollection < ::GroupSchemas::IdiomCollection
    self.table_name = 'group_schemas_idiom_collections'
  end
end
