# frozen_string_literal: true

require 'idioms_source/engine'

module IdiomsSource
  class Hooks
    REFRESH_INTERVAL = '2h'

    # @return [String]
    def self.refresh_interval
      REFRESH_INTERVAL
    end

    # @param [Hash] configuration
    def initialize(instance_id, configuration)
      @instance_id = instance_id
      @configuration = configuration
    end

    def default_title
      @configuration['language'] ? I18n.t("idioms_source.title.#{@configuration['language']}") : I18n.t('idioms_source.invalid')
    end

    def configuration_valid?
      @configuration['language'].present? && !@configuration['language'].empty?
    end

    def list_sub_resources
      [["wikiquote_csv_#{@configuration['language']}", default_title]]
    end

    def fetch_data(_group, _sub_resources)
      language = @configuration['language'] || 'en'

      collection = IdiomCollection.find_or_initialize_by(id: "wikiquote_csv_#{language}") do |collection|
        collection.id = "wikiquote_csv_#{language}"
        collection.collection_name = "Source unknown, scraped from #{language}.wikiquote.org"
      end

      idioms = CSV.read(IdiomsSource::Engine.root.join("lib/data/#{language}.csv"), col_sep: '|').sample(120)
      collection.items.each(&:mark_for_destruction)
      idioms.map.with_index do |idiom, index|
        message = idiom.first
        author = idiom.second
        collection.items.new(
          uid: "#{language}_#{index}",
          message: message,
          author: message.eql?(author) ? nil : author,
          language: language
        )
      end

      [collection]
    end
  end
end
