# frozen_string_literal: true

require 'test_helper'

class IdiomsSource::Test < ActiveSupport::TestCase
  test 'truth' do
    assert_kind_of Module, IdiomsSource
  end
end
